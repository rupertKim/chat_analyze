var express = require('express');
var fs = require('fs');
var request = require('request');

var router = express.Router();
var multer  = require('multer');

var done=false;
var file_n = 'a';
router.use(multer({ dest: './public/uploads/',
    rename: function (fieldname, filename) {
        return filename+Date.now();
    },
    onFileUploadStart: function (file) {
        console.log(file.originalname + ' is starting ...')
    },
    onFileUploadComplete: function (file) {
        console.log(file.fieldname + ' uploaded to  ' + file.path);
        //console.log(file);
        file_n=file.path;
        done=true;
    }
}));

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('index', { title: 'Express' });


});
router.get('/help', function(req, res, next) {

    res.render('help', { title: 'Express' });


});
router.get('/chart', function(req, res, next) {

    res.render('chart', { title: 'Express' });


});
router.get('/test', function(req, res, next) {

    res.render('test', { title: 'Express' });


});
router.post('/', function(req, res){

    var device = req.query.device;
    //console.log(req.files.myfile.name);
    console.log('전송');
    request.get('http://localhost:3000/process/write',{form:{id:req.files.myfile.name}} ,function (error, response, body) {
        var data_id = JSON.parse(response.body);
        var data_id = data_id.result;
        if (!error && response.statusCode == 200) {
            setTimeout(function () {
                request.get('http://114.70.192.22:3000/process/chart/'+JSON.parse(body).result,"", function (error, response, body) {
                //request.get('http://localhost:3000/process/chart/'+JSON.parse(body).result,"", function (error, response, body) {

            });
            },5000);
            if(!!device && device=="mobile") {
                res.json({"res":data_id});
            }
            else{
                console.log('success');
                //console.log(body);
                res.redirect('/?id='+data_id);
            }
        }else{
            console.log('fail');
            console.log(error);
            //console.log(body);
        }
    });
});


module.exports = router;
