/**
 * Created by yoonsKim on 15. 7. 22..
 */
var express = require('express');
var request = require('request');
var async = require('async');
var HashMap = require('hashmap');

var router = express.Router();


var mongoose = require('mongoose');
mongoose.createConnection('mongodb://114.70.192.22/chat');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log('CONNECTED MONGO');
});
// create schema
var chat_child = new mongoose.Schema({ time: Date, name: String });
var pkg_child = new mongoose.Schema(
    {
        start_date:Date,
        end_date:Date,
        msg:String,
        chat_log:[chat_child]
    });
var ChatSchema = new mongoose.Schema({
    file:String,
    title:String,
    chart_per:Number,
    chart_data:String,
    lang_data:String,
    pkg:[pkg_child]
});
var Chat = mongoose.model('mymodel',ChatSchema,'chat');

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

router.get('/', function(req, res, next) {
    var name = req.query.id;

    var query = Chat.find();
    query.where('_id').equals(name);
    query.select('title pkg.start_date pkg.chat_log.name');
    query.exec(function (err, result) {
        result = result[0];
        res.json(result);

    });



});
router.get('/chart', function(req, res, next) {

    var name = req.query.id;
    var query = Chat.find();
    query.where('_id').equals(name);
    query.select('chart_data');
    query.exec(function (err, result) {
        result=result[0];
        result = result.chart_data;
        if(!result){
            res.json({"res":"null"});

        }
        res.json(eval(result));

    });

});

router.get('/lang_top/:num', function(req, res, next) {
    var name = req.query.id;

    var query = Chat.find();
    query.where('_id').equals(name);
    query.select('lang_data');
    query.exec(function (err, result) {
        result=result[0];
        result = result.lang_data;
        if(!!result){
            console.log(result);
            res.json(eval(result));
            return;
        }else{

            var top_num = req.params.num;
            var all_msg = "";
            var word_limit = 20000;
            var query = Chat.find({});

            query.where('_id').equals(name);


            query.select('pkg.msg');
            query.exec(function (err, result) {
                var msg = result[0];
                for(var i=msg.pkg.length-1;i>=0;i--){
                    all_msg+=msg.pkg[i].msg;
                    if(all_msg.length>word_limit){
                        break;
                    }
                }
                request.post('http://114.70.192.22:8080/nlp/hi.jsp',{form:{msg:all_msg}}, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        body = JSON.parse(body);

                        for(var i=0;i<body.length; i++){
                            for(var k=0;k<body.length;k++){
                                if(i==k){
                                    continue;
                                }
                                if(body[k].word.search(body[i].word)!=-1){
                                    body[i].weight=0;
                                    break;
                                }
                            }
                        }
                        body.sort(function(a, b) {
                            return parseInt(b.weight) - parseInt(a.weight);
                        });



                        if(body.length>top_num){
                            body = body.splice(0,top_num);
                        }

                        Chat.update({_id:name},{$set:{lang_data:JSON.stringify(body)}}, function (err, tank) {
                            console.log(tank);
                        });
                        //console.log(body);
                        res.json(body);

                    }else{
                        console.log(body);
                        res.json('err');
                    }
                });


            });

        }

    });



});




router.get('/lang/:num', function(req, res, next) {

    var name = req.query.id;

    var num = parseInt(req.params.num);
    var query = Chat.find({});
    query.where('_id').equals(name);

    query.select('pkg.msg pkg.start_date');
    var arr = {};
    arr.pkg=[];
    arr.time=[];
    query.exec(function (err, result) {
        var msg = result[0];
        var fin = msg.pkg.length-num-1;

        for(var i=msg.pkg.length-1;i>fin && i>=0;i--){
            request.post('http://114.70.192.22:8080/nlp/hi.jsp',{form:{msg:msg.pkg[i].msg}}, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    body = JSON.parse(body);
                    body.sort(function(a, b) {
                        return parseInt(b.count) - parseInt(a.count);
                    });
                    if(body.length>10){
                        body = body.splice(0,10);
                    }

                    arr.pkg.push(body);
                    if(arr.pkg.length==num){
                        for(var i =0;i<num;i++){
                            arr.time.push(msg.pkg[fin-i].start_date);
                        }
                        res.json(arr);
                    }
                }else{
                    res.json(body);
                }
            });
        }

    });

});


router.get('/follower/:id/:filt_name', function(req, res, next) {

    var name = req.params.id;
    var filt_name = req.params.filt_name;
    //var name = req.query.id;
    console.log(name,filt_name);
    var query = Chat.find();

    query.where('_id').equals(name);
    query.select('pkg.chat_log.name');
    var min_filter=1.5;
    var std_min_per = 1;

    query.exec(function (err, result) {
        result = result[0];

        //var arr_name=getNameListTest(result);
        var arr_name_map=getNameList(result);
        filtNameList(arr_name_map,name,std_min_per, function (map) {
            arr_name_map =map;

            var arr_value=[];
            arr_value = getChartList(result,arr_name_map,filt_name);
            res.json(arr_value);

        });
    });




    function getChartList(result,arr_name_map,my_name) {
        var pkg = result.pkg;
        var arr=[];
        var arrMap = new HashMap();

        var cnt_variation = 6;
        var cnt_variation_dupl = 5;



        for(var i in pkg){
            var inner = pkg[i].chat_log;
            for(var k in inner){
                if(!inner[k].name){
                    continue;
                }
                var inner_name = inner[k].name;
                if(inner_name==my_name) {
                    var idx=0;
                    var weight=30;
                    var weight_dupl=10;
                    var weight_desc=8;

                    for(var z=idx; z<idx+cnt_variation || z<inner.length; z++){
                        if(!inner[z]){
                            continue;
                        }

                        if(inner[z].name==my_name){
                            idx=z;
                            weight=weight_dupl;
                            continue;
                        }


                        var value = arrMap.get(inner[z].name);
                        if(value==undefined){
                            value=1;
                        }
                        value+=(weight*weight);

                        arrMap.set(inner[z].name,value);


                        //if(isNot){
                        //    arr.push([my_name,inner[z].name,(weight*weight)]);
                        //
                        //}
                        weight<=1?weight+=0:weight-=weight_desc;

                    }
                    k=z;//이동한 만큼 넘긴다.
                }
            }
        }
        arrMap.forEach(function(value, name) {

            var m_value = arr_name_map.get(name);
            if(!m_value){

            }
            else{
                value/=m_value;
                if(value>min_filter){

                    arr.push([my_name,name,value]);
                }
            }


        });



        arr.sort(function (a, b) {
           return b[2]-a[2];
        });

        return arr;


    }


    function filtNameList(name_map,my_name,std_min_per,callback){
        var all = name_map.get("$all");
        name_map.forEach(function (v, name) {
            if(name==my_name){

            }else{
                var per_value=v * 100/all;
                if(per_value<std_min_per){
                    name_map.remove(name);
                }
            }
        });
        callback(name_map);
    }

    function getNameList(result) {

        var nameMap = new HashMap();
        var len=0;
        var pkg = result.pkg;
        if(!result.pkg){
            return "";
        }
        for(var i in pkg){
            var chat_log = pkg[i].chat_log;
            if(!chat_log){
                continue;
            }
            len+=chat_log.length;
            for(var k=0;k< chat_log.length;k++){

                var name = chat_log[k].name;
                var value = nameMap.get(name);

                if(value==undefined){
                    value=1;
                }else{
                    value++;
                }

                nameMap.set(name,value);
            }

        }
        //$all은 전체 대화량.
        nameMap.set("$all",len);
        len = nameMap.get("$all");
        nameMap.forEach(function(value, name) {
            //var weight = value/len*100;
            var weight = value;
            if(weight<min_filter){
                nameMap.remove(name);
            }else{
                nameMap.set(name,weight);
            }
        });


        return nameMap;

    }
    function indexof(arr,key_value,val_value){
        for(var i in arr){
            var ob = arr[i];
            if(ob[key_value]==val_value){
                return i;
            }
        }
        return -1;
    }


});
module.exports = router;
