var express = require('express');
var request = require('request');
var async = require('async');
var router = express.Router();
var fs = require('fs');

var mongoose = require('mongoose');
mongoose.connect('mongodb://114.70.192.22/chat');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log('CONNECTED MONGO');
});
// create schema
var chat_child = new mongoose.Schema({ time: Date, name: String });
var pkg_child = new mongoose.Schema(
    {
        start_date:Date,
        end_date:Date,
        msg:String,
        chat_log:[chat_child]
    });
var ChatSchema = new mongoose.Schema({
    file:String,
    title:String,
    chart_per:Number,
    chart_data:String,
    lang_data:String,
    pkg:[pkg_child]
});
var Chat = mongoose.model('chat',ChatSchema,'chat');
var MIN=60000;

String.prototype.replaceAll = function(org, dest) {
    return this.split(org).join(dest);
};
router.get('/write', function(req,res,err){
    var f_name = req.body.id;
    if(f_name ==undefined){
        res.json('err');
    }
    var arr_pkg = [];
    var std_time=new Date("1980-01-01 00:00").getTime();
    var std_interver = 50*MIN;
    var std_length=5;
    var title="";

    var arr = {};
    arr.start_date="";
    arr.end_date="";
    arr.msg = "";
    arr.chat_log = [];

    var name_table=[];
    function Name(name,idx){
        return {"name":name,"idx":idx};
    }
    fs.readFile('public/uploads/'+f_name, function (err,data) {
        console.log(err);
        data = data.toString();
        var reg_get_data = /([\d]{4}\D \d*\d\D \d\d\D \D\D \d\d*:\d\d), ([^:]*) : (.*)/;


        var array = data.split("\n");
//    대화뭉치 나누기........
        title = array[0];
        for(i in array) {
            var element = reg_get_data.exec(array[i]);

            if(element== null){
                continue;
            }

            var time = element[1];
            var name = element[2];
            var msg = element[3];



            if(getTime(time)==false){
                continue;
            }

            msg = filter_msg(msg);

            if(msg.length==0){
                continue;
            }
            time=getTime(time);
            var input ={};
            input.time = time;
            input.name = name;
//        input.msg= msg;

            if(std_time+std_interver<time){

                if(arr.chat_log.length>std_length){
                    arr_pkg.push(arr);
                }
                arr={};
                arr.start_date=time;
                arr.end_date="";
                arr.msg = "";
                arr.chat_log = [];
                std_time = time;
            }
            std_time=time;
            arr.end_date = time;
            arr.msg+= msg+" ";
            arr.chat_log.push(input);
        }
        arr_pkg.push(arr);

//        for(var i in arr_pkg ){
//            console.log("---");
//            console.log(arr_pkg[i].msg);
//        }

        console.log('-- 파일분석 finished --');
        var memo = new Chat({title: title,file:f_name  ,pkg:arr_pkg,chart_per:0,chart_data:"",lang_data:""});

        memo.save(function(err,silence){
            if(err){
//                console.log(err);
                throw err;
            }


            res.json({"result":silence._id});
        });
    });

    function filter_msg(input) {
        if(input=="<사진>" || input=="(이모티콘)" || input=="사진"){
            input="";

        }
        input = input.replace(/ㅋ*/g,"");
        input = input.replace(/(이모티콘)/,"");
        input = input.replace(/ㅇㅇ/,"");
        input = input.replace(/\.{3,}/,".");
        input = input.replace(/\,{3,}/,",");
        input = input.replace(/~{2,}/,"~");
        input = input.replace(/ㅠ{3,}/,"");
        input = input.replace(/ㅜ{3,}/,"");
        return input;

    }


    function getTime(time) {
        var reg_time_split = /([\d]{4})\D (\d*)\D (\d*)\D (\D\D) (\d*):(\d*)/;
        time = reg_time_split.exec(time);
        if(time==null){
            return false;
        }
        if(time[4]=="오후"&&time[5]!=12){
            time[5]=parseInt(time[5])+12;
        }
        else if(time[4]=="오전"&&time[5]==12){
            time[5]=0;
        }

        return new Date(time[1]+"-"+time[2]+"-"+time[3]+" "+time[5]+":"+time[6]).getTime();
    }


});

router.delete('/write/:id', function(req,res,err){
    var name = req.params.id;

    console.log(name);
    if(name  ==undefined){
        res.json({"res":"err"});
        return;
    }
    Chat.find({ _id: name}).remove(function (e, response) {
        console.log(e);
        if(e!=null){
            res.json({"res":"err"});
            return;
        }
        console.log("what?");
        res.json(response);
        return;
    } );


});


router.get('/chart/:id', function(req, res, next) {

var name = req.params.id;
    //var name = req.query.id;
    var query = Chat.find();

    query.where('_id').equals(name);
    query.select('pkg.chat_log.name');
        query.exec(function (err, result) {
            result = result[0];
            var arr_name = getNamePkg(result);
            var arr_value=[];

            for(var i in arr_name){
                if(!arr_name[i]){
                    continue;
                }
                arr_value.push(getChartName(result,arr_name[i]));
                var per = Math.round(i / arr_name.length*100);

                console.log(per+"%");



            }
            var arr_temp=[];
            for(var i in arr_value ){
                var value = arr_value[i];

                for(var k in value ){
                    arr_temp.push(value[k]);
                }
            }
            arr_value = arr_temp;
            var arr_string = JSON.stringify(arr_value);
            Chat.update({_id:name},{$set:{chart_per:100}}, function (err, tank) {
                console.log(tank);
            });
            Chat.update({_id:name},{$set:{chart_data:arr_string}}, function (err, tank) {
                console.log(tank);
            });

            res.json(arr_value);
        });




    function getChartName(result,my_name) {
        var pkg = result.pkg;

        var arr=[];
        var cnt_variation = 5;
        var cnt_backstep = 2;//내 메시지를 찾았을 때 그보다 해당 숫자만큼 앞으로 돌림

        for(var i in pkg){
            var inner = pkg[i].chat_log;
            for(var k in inner){
                if(!inner[k].name){
                    continue;
                }
                var inner_name = inner[k].name;
                var isNot=true;
                if(inner_name==my_name){
                    var idx=0;
                    (k-cnt_backstep)<0?idx=0 : idx =k-cnt_backstep;
                    var avoidList=[];
                    for(var z=idx; z<idx+cnt_variation || z<inner.length; z++){
                        if(!inner[z]){
                            continue;
                        }
                        if(inner[z].name==my_name){
                            idx=z;
                            continue;
                        }
                        var isNot=true;
                        for(var j in arr){
                            if(arr[j][1] ==inner[z].name){
                                var weight=1;
                                if(avoidList.indexOf(inner[z].name)==-1){
                                    weight=10;
                                    avoidList.push(inner[z].name);
                                }
                                arr[j][2]+=weight;
                                isNot=false;
                                break;
                            }
                        }
                        if(isNot){
                            var weight = 1;
                            if(avoidList.indexOf(inner[z].name)==-1){
                                weight=10;
                                avoidList.push(inner[z].name);
                            }
                            arr.push([my_name,inner[z].name,weight]);
                        }
                    }
                }
            }
        }
        for(var i in arr){
            arr[i][2]=Math.round(arr[i][2]/10);
        }

        return arr;


    }

    function getNamePkg(result) {
        var arr = [];
        var arr_name = [];
        var filter_list = ["$shift","notify","indexOf","","addToSet","hasAtomics","set","splice","null",null];
        var pkg = result.pkg;
        if(!result.pkg){
            return "";
        }
        for(var i in pkg){
            var chat_log = pkg[i].chat_log;
            for(var k in chat_log){
                arr_name.push(chat_log[k].name);
            }

        }
        for(var i in arr_name ){
            if(filter_list.indexOf(arr_name[i]) != -1){
                continue;
            }
            if(arr.indexOf(arr_name[i])==-1){
                arr.push(arr_name[i]);
//                arr_name = removeA(arr_name,arr_name[i]);
//                i=0;
            }
        }
        delete arr_name;
        arr.sort();
        return arr;
    }


});







module.exports = router;


