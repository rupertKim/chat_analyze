

var id= getQueryVariable("id");

app.controller("index", function ($scope, $rootScope,$http) {
    $scope.loaded_nlp=false;
    $scope.bg_nlp = "/assets/img/bg/wood.jpg";

    if(id==null || !id){
        $("#file_modal").modal();
        return;
    }
    $("#load_modal").modal();


    $http({
        method: 'GET',
        url: 'http://114.70.192.22:8888/bgimg/'
    }).success(function (data, status, headers, config) {
        $scope.bg_nlp = data.url;
    }).error(function (data, status, headers, config) {
        $scope.bg_nlp = "/assets/img/bg/wood.jpg";
    });


    $http({
        method: 'GET',
        url: '/info/lang_top/40',
        params:{id:id}
    }).success(function (data, status, headers, config) {
            if(!data || data=="err"){
                return;
            }

            var WEIGHT_MULTIPLE = 700;
            data.sort(function(a, b) {
                return parseInt(b.weight) - parseInt(a.weight);
            });
            var fst = data[0];

            var fill = d3.scale.ordinal()
                .range(["#C8DBDE","#e74c3c","#22A7F0","#D2527F","#03C9A9"]);
            var cnt=0;
            for(var i in data){
                cnt+=data[i].weight;
            }

            WEIGHT_MULTIPLE/=cnt;
            $scope.$emit("nlp_data",data);


            d3.layout.cloud().size([300, 250])
                .words(data.map(function(d) {
                    return {text: d.word, size: d.weight*WEIGHT_MULTIPLE};
                }))
                .padding(2)
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", draw)
                .start();
            $scope.loaded_nlp=true;


        function draw(words) {
                d3.select("#unseen").append("svg")
                    .attr("width", 300)
                    .attr("height", 280)
                    .append("g")
                    .attr("transform", "translate(150,150)")
                    .selectAll("text")
                    .data(words)
                    .enter().append("text")
                    .style("font-size", function(d) { return d.size + "px"; })
                    .style("font-family", "Impact")
                    .style("fill", function(d, i) { return fill(i); })
                    .attr("text-anchor", "middle")
                    .attr("transform", function(d) {

                        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                    })
                    .text(function(d) { return d.text; });
            }
        })
        .error(function (data, status, headers, config) {
            console.log(data);
        });

    $http({
        url: "/info",
        params: { id:id }
    })
        .success(function( data ) {
            if(!data){
                return;
            }
            $("#load_modal").modal("hide");

            var chat_pkg_len=0;
            var chatter_len=0;
            data.title = data.title.replace(/.txt/,"");
            var date = data.pkg[0].start_date;
            date = new Date(date).convert();
            chat_pkg_len = data.pkg.length;

            $scope.title = data.title;
            $rootScope.main_title = data.title;
            $scope.date = date;

            var arr = getNamePkg(data);
            var leader = getLeader(data);
            $scope.leader_name = leader.name;
            $scope.leader_count = leader.cnt;

            chatter_len = arr.length;
            $scope.top_chatter_name=arr[0].name;
            //$scope.top_chatter_count=arr[0].count;
            $scope.top_chatter_count=arr[0].count/2;
            var tmp = arr[0].count;
            var cnt=64;
            var timer = setInterval(function () {
                if(tmp>$scope.top_chatter_count){
                    $scope.top_chatter_count+=Math.round(tmp/cnt);
                    cnt+=3;
                    $scope.$apply();
                }else{
                    $scope.top_chatter_count=tmp;
                    $scope.$apply();
                    clearTimeout(timer);
                }
            },15);




            var chatter_chart_arr=[];
            for(var i in arr){

                var name= arr[i].name;
                var y = arr[i].count;

                var ob;
                if(i==0){
                    ob = {"name":name,"y":y, "sliced": true,
                        "selected": true};
                }else{
                    ob = {"name":name,"y":y};
                }
                chatter_chart_arr.push(ob);
            }
            HIGH_CHART_PIE.series[0].data=chatter_chart_arr;
            $('#chatter_chart').highcharts(HIGH_CHART_PIE);

        });

    function getNamePkg(result) {
        var arr = [];



        var pkg = result.pkg;
        for(var i in pkg){
            var inner = pkg[i].chat_log;
            for(var k in inner){
                var inner_name = inner[k].name;
                var isNot=true;
                if(!inner_name || inner_name=="notify" || inner_name=="indexOf"){
                    continue;
                }
                for(var j in arr){
                    if(arr[j].name ==inner[k].name){
                        arr[j].count++;
                        isNot=false;
                        break;
                    }
                }
                if(isNot){
                    arr.push({"name":inner[k].name,"count":1});
                }

            }

        }
        arr.sort(function(a, b) {
            return parseInt(b.count) - parseInt(a.count);
        });
        return arr;
    }
    function getLeader(result) {
        var arr = [];
        var Leader = function () {
            return {"name":"","cnt":0};
        };


        var pkg = result.pkg;
        for(var i in pkg){
            var inner = pkg[i].chat_log;

            var rank_num = 0;
            var dupl_name_arr = [];

            for(var k in inner){
                if(rank_num==3){
                    break;
                }

                var inner_name = inner[k].name;
                if(!inner_name || inner_name=="notify" || inner_name=="indexOf"){
                    continue;
                }



                if(dupl_name_arr.indexOf(inner[k].name)!=-1){
                    continue;
                }

                dupl_name_arr.push(inner_name);

                var isNot=false;
                for(var j in arr){
                    if(arr[j].name ==inner[k].name){
                        switch (rank_num){
                            case 0:
                                arr[j].cnt += 8;
                                break;
                            case 1:
                                arr[j].cnt += 4;
                                break;
                            case 2:
                                arr[j].cnt += 1;
                                break;
                        }
                        rank_num++;
                        isNot=true;
                        break;
                    }
                }

                if(isNot){
                    continue;
                }

                var ob = Leader();
                ob.name = inner_name;
                switch (rank_num){
                    case 0:
                        ob.cnt += 8;
                        break;
                    case 1:
                        ob.cnt += 4;
                        break;
                    case 2:
                        ob.cnt += 1;
                        break;
                }
                rank_num++;

                arr.push(ob);




            }

        }
        arr.sort(function(a, b) {
            return parseInt(b.cnt) - parseInt(a.cnt);
        });
        return arr[0];
    }


    var HIGH_CHART_PIE={
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "채팅량",
            colorByPoint: true,
        }]
    };


    api.info.follower(null,null, function (data, status) {

        var avg=0;
        $scope.followerList=[];
        var line_char_list = ["brown-bg.png","james-bg.png","moon-bg.png","sally-bg.png","brown.png","cony.png","james.png","moon.png","sally.png"];
        console.log(data.length);

        for(var i in data){
            if(i==0 || i==data.length-1){
                continue;
            }
            console.log(data[i][2]);
            avg+=data[i][2];
        }
        avg/=data.length-2;
        for(var i in data){
            if(data[i][2]<avg){
                break;
            }
            data[i][0]=line_char_list[i%(line_char_list.length)];
            $scope.followerList.push(data[i]);
        }


    },[id,"회원님"]);



});
