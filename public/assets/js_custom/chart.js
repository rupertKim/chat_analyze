
var id= getQueryVariable("id");




$(document).ready(function () {
    if(id==null || !id){
        location.href = "/";
        return;
    }
    $("#load_modal").modal();
    $.ajax({
        url: "/info",
        data: { id:id }
    })
        .done(function( data ) {
            if (!data) {
                return;
            }
            $("#load_modal").modal("hide");
            data.title = data.title.replace(/.txt/,"");
            var date = data.pkg[0].start_date;
            date = new Date(date).convert();


            $("#main-content .head").text(data.title);
            $("#main-content .start_date").text(date);



    });
    $.ajax({
        url: "/info/chart",
        data: { id:id }
    })
        .done(function( data ) {
            if (!data) {
                return;
            }
            $("#load_modal").modal("hide");
            if(data.res=="null"){
                $("#err_modal").modal();
                return;
            }

            var sales_data=data;

            var width = 500, height = 610, margin ={b:0, t:40, l:150, r:50};

            var svg = d3.select("body #main-content #chatter .pn .chart")
                .append("svg").attr('width',width).attr('height',(height+margin.b+margin.t))
                .append("g").attr("transform","translate("+ margin.l+","+margin.t+")");

            var data = [
                {data:bP.partData(sales_data,2), id:'SalesAttempts', header:["이름","친한 사람", ""]}
            ];
            setTimeout(function () {
                $("#SalesAttempts .part1 .barvalue").attr("x",120);
                $("#SalesAttempts .part1 .barpercent").attr("x",160);
                //$("svg").css("width","100%");
            },50);

            bP.draw(data, svg);



    });



});