/**
 * Created by yoonsKim on 15. 7. 2..
 */

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return null;
//    alert('Query Variable ' + variable + ' not found');
}
Date.prototype.convert = function () {
    if (!this.valueOf()) return " ";
    var time = this;

    var tmp="";
    //tmp = (time.getFullYear());
    //tmp+="-";
    tmp+=(time.getMonth()+1).zf(2);
    tmp+="-";
    tmp+=time.getDate();
    tmp+=" ";
    tmp+=time.getHours().zf(2);
    tmp+=":";
    tmp+=time.getMinutes().zf(2);
    return tmp;
};

String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};

var id= getQueryVariable("id");

var app =angular.module("analyize", []);
var ajax="";
var api={};

app.controller("parent", function ($scope, $rootScope,$http) {
    $rootScope.main_title="";

    Kakao.init('44e4e20ac4f533897ad8f91a8071d243');
    $scope.$on("nlp_data", function (e,nlp_data) {
        var kakao_msg="카톡방 사용 단어순위\n";
        for(var i=0; i< nlp_data.length&&i< 5 ; i++){
            kakao_msg+= (parseInt(i)+1)+"위 : "+ nlp_data[i].word+"("+nlp_data[i].weight+"회)\n";
        }
        // 카카오톡 링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
        Kakao.Link.createTalkLinkButton({
            container: '#share',
            label: kakao_msg,
            image: {
                src: 'http://hitit.jbnu.ac.kr:3000/assets/img/default/bg_kakao.jpg',
                width: '300',
                height: '200'
            },
            webButton: {
                text: '단톡방 분석기 확인',
                url: 'http://hitit.jbnu.ac.kr:3000?id='+id // 앱 설정의 웹 플랫폼에 등록한 도메인의 URL이어야 합니다.
            }
        });
    });

    ajax = function(method, url, async) {

        return function(data,params, callback, url_params,optional_params){

            for(var i in url_params){
                url+=url_params[i]+"/";
            }
            var options = {
                method: method,
                async: async,
                dataType: "json"
            };


            var req = {
                method: method,
                url: url,
                data: data,
                params:params
            };
            console.log(req);
            $http(req).success(callback).error(callback);
        };
    };

    api.info={};
    api.info.follower=ajax("get","/info/follower/");


});

$(document).ready(function () {
    if(id=='null'){
        location.href = '/';
    }
    $("#nav-accordion .dashboard>a").attr("href","/?id="+id);
    $("#nav-accordion .chart-section>a").attr("href","/chart?id="+id);
    $(".href-chart").attr("href","/chart?id="+id);
    $(".href-help").attr("href","/help?id="+id);
    $(".href-help-upload").attr("href","/help?id="+id+"#info-upload");
    $(".href-help-chart").attr("href","/help?id="+id+"#info-chart");
    $(".header .logo").attr("href","/?id="+id);



    $(".tooltips").click(function () {
        if($("#nav-accordion").hasClass("none")){
            $("#nav-accordion").removeClass("none");
        }else{
            $("#nav-accordion").addClass("none");
        }
    })

});
    // 사용할 앱의 Javascript 키를 설정해 주세요.
